import '../styles/globals.css'

function MyApp({ Component, pageProps, myProp}) {
  return <Component extraThing="yep" {...pageProps} >
      {myProp}
    </Component>
}

export default MyApp
