import React from "react"

const PrevButton = ({ prevId, onClick }) => {
    return (
    <div>
        <a href={`#${prevId}`} onClick={onClick}>Previous</a>
        </div>
    )   
}

export default PrevButton