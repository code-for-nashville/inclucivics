import React from "react"
import InfoSection from "../InfoSection/InfoSection"

const Problem = ({ problem, id, nextId, prevId, onClick }) => {
    return (
    <div>
        <InfoSection id={id} nextId={nextId} prevId={prevId} onClick={onClick}>
            <div>{problem.text}</div>
        </InfoSection>
        </div>
    )
}

export default Problem