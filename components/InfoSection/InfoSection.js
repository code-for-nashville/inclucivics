import React from "react";
import SectionNav from "../SectionNav"
import styles from "./InfoSection.module.css"

const InfoSection = ({ id, prevId, nextId, onClick, children }) => {
  return (
    <div id={id} className={styles.info_section}>
          {children}
          <SectionNav prevId={prevId} nextId={nextId} onClick={onClick}/>
    </div>
  );
};

export default InfoSection
