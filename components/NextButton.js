import React from "react"

const NextButton = ({ nextId, onClick}) => {
    return (
        <div>
            <a href={`#${nextId}`} onClick={onClick}>Next</a>
        </div>
    )
}

export default NextButton